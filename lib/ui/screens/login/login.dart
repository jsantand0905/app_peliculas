import 'package:flutter/material.dart';
import 'package:movieapp/config/app_config.dart';
import 'package:movieapp/design_system/button_app.dart';
import 'package:movieapp/design_system/colors.dart';
import 'package:movieapp/design_system/enumarables.dart';
import 'package:movieapp/design_system/page_template/page_template.dart';
import 'package:movieapp/design_system/responsive_design.dart';
import 'package:movieapp/design_system/snackbar_message.dart';
import 'package:movieapp/design_system/spacing.dart';
import 'package:movieapp/design_system/text_app.dart';
import 'package:movieapp/design_system/tipography.dart';
import 'package:movieapp/ui/interface/base_interface.dart';
import 'package:movieapp/ui/models/ui_app_model.dart';
import 'package:movieapp/ui/presenter/base_presenter.dart';
import 'package:movieapp/ui/screens/home/home.dart';
import 'package:movieapp/ui/screens/main/main_view.dart';

class LoginScreen extends StatefulWidget {

  final AppConfig config;
  final Map<String, dynamic> language;

  LoginScreen({
    Key? key,
    required this.config,
    required this.language
  }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> implements BaseInterface{

  late BasePresenterApp _basePresenterApp;
  UiAppModel _uiAppModel = UiAppModel();
  late SnackbarMessage _snackbarMessage; 
  late ResponsiveDesign _responsiveDesign; 
  TextEditingController _editingControllerPassword = TextEditingController();
  TextEditingController _editingControllerName= TextEditingController();
  List<String> _names = ['maria','pedro'];
  List<String> _passwords = ['password','123456'];

  @override
  void initState() {
    _basePresenterApp = BasePresenterApp(this);
    _basePresenterApp.getUi(widget.language);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _responsiveDesign = ResponsiveDesign(context);
    _snackbarMessage = SnackbarMessage(context);
    return PageTemplate(
      viewImageBackground: true,
      bodyTemplate: Container(
        padding: EdgeInsets.only(top: _responsiveDesign.heightMultiplier(50)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextApp(
              textType: TextType.White,
              size: TypographyApp.FONT_SIZE_H3,
              text: _uiAppModel.uiWelcomeModel!.welcome + "!",
              align: TextAlign.center ,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: _responsiveDesign.widthMultiplier(30),
                vertical: _responsiveDesign.heightMultiplier(35)),
              decoration: BoxDecoration(
                color: ColorsApp.COLOR_BLACK.withOpacity(0.97),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50),
                  topRight: Radius.circular(50)
                )
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment : MainAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.close,
                        color: ColorsApp.COLOR_WITHE,
                      ),
                    ],
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(labelText: _uiAppModel.uiLoginModel!.name),
                    controller: _editingControllerName,
                  ),
                  SizedBox(
                    height: _responsiveDesign
                        .heightMultiplier(Spacing.SPACE_XL),
                  ),
                  TextFormField(
                    controller: _editingControllerPassword,
                    obscureText: true,
                    decoration: InputDecoration(labelText: _uiAppModel.uiLoginModel!.password),
                  ),
                  SizedBox(
                    height: _responsiveDesign
                        .heightMultiplier(Spacing.SPACE_XL),
                  ),
                  ButtonApp(
                    pressed: () => _validateLogin(
                      _editingControllerName.text.trim(), 
                      _editingControllerPassword.text.trim()), 
                    value: _uiAppModel.uiWelcomeModel!.login,
                    typeButton: ButtonType.Secondary,
                    width: _responsiveDesign.widthMultiplier(180),
                  ),
                ],
              ),
            )
          ],
        ),
      )
    );
  }

  _validateLogin(String name, String password){
    int pos =_names.indexOf(name);
    if(pos != -1 && _passwords[pos] == password){
      _goToHome();
    }else{
      _snackbarMessage.showSnackbar(
        message: _uiAppModel.uiLoginModel!.messageError
      );
    }
  }

  void _goToHome(){
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
        builder: (BuildContext context) => MainView(
          config: widget.config,
          language: widget.language,
        )),(value){return false;});
  }

  @override
  void setUi(UiAppModel model) {
    _uiAppModel = model;
    setState(() {
      
    });
  }
}