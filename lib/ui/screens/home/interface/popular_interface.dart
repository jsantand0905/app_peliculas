import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/domain/model/home/response_popular.dart';
import 'package:movieapp/ui/interface/base_interface.dart';

abstract class PopularInterface extends BaseInterface{
  void getInformation(ResponsePopular model, int? flag);
  void showError(ErrorItem error);
  void showLoading(int? flag);
  void hideLoading(int? flag);
}