import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:movieapp/design_system/button_app.dart';
import 'package:movieapp/design_system/colors.dart';
import 'package:movieapp/design_system/enumarables.dart';
import 'package:movieapp/design_system/responsive_design.dart';
import 'package:movieapp/design_system/spacing.dart';
import 'package:movieapp/design_system/text_app.dart';
import 'package:movieapp/design_system/tipography.dart';
import 'package:movieapp/domain/model/home/popular_model.dart';

class ItemRecomendation extends StatefulWidget {

  
  final PopularModel recomedationsModel;
  final IconData? icon;
  final VoidCallback clickFavorite;

  const ItemRecomendation({
    Key? key,
    required this.recomedationsModel,
    this.icon,
    required this.clickFavorite
  }) : super(key: key);

  @override
  _ItemRecomendationState createState() => _ItemRecomendationState();
}

class _ItemRecomendationState extends State<ItemRecomendation> {

  late ResponsiveDesign _responsiveDesign;
  late IconData _iconData;
  late Color _colorIcon;

  @override
  void initState() {
    _iconData = widget.icon ?? Icons.favorite_outline;
    _colorIcon = widget.icon == null ? ColorsApp.COLOR_GRAY : ColorsApp.COLOR_YELLOW;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _responsiveDesign = ResponsiveDesign(context);
    return Container(
      margin: EdgeInsets.only(top: _responsiveDesign.heightMultiplier(20)),
      width: MediaQuery.of(context).size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //Image.network(Endpoints.baseUrlImage + 'w500/vC324sdfcS313vh9QXwijLIHPJp.jpg'),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextApp(
                textType: TextType.White, 
                size: TypographyApp.FONT_SIZE_H5, 
                text: widget.recomedationsModel.name!,
              ),
              SizedBox(
                height: _responsiveDesign.heightMultiplier(Spacing.SPACE_MD),
              ),
              RatingBar.builder(
                itemSize: TypographyApp.FONT_SIZE_H5,
                itemCount: 5,
                initialRating: widget.recomedationsModel.voteAverage!/2,
                glowColor: ColorsApp.COLOR_WITHE,
                itemBuilder:  (context, _) => Icon(
                  Icons.star,
                  color: ColorsApp.COLOR_YELLOW,
                ), 
                onRatingUpdate: (value){}
              ),
              SizedBox(
                height: _responsiveDesign.heightMultiplier(Spacing.SPACE_MD),
              ),
              TextApp(
                textType: TextType.Gray, 
                size: TypographyApp.FONT_XS,
                text: 'IMDB: ${widget.recomedationsModel.voteAverage!}',
              ),
              SizedBox(
                height: _responsiveDesign.heightMultiplier(Spacing.SPACE_MD),
              ),
              ButtonApp(
                pressed: (){}, 
                value: 'Watch now',
                typeButton: ButtonType.Primary,
              ),
            ],
          ),
          GestureDetector(
            onTap: (){
              if(widget.icon == null){
                _saveFavorite();
              }
              widget.clickFavorite.call();
            },
            child: Icon(
              _iconData,
              size: TypographyApp.FONT_SIZE_H2,
              color: _colorIcon,
            ),
          )
        ],
      ),
    );
  }

  _saveFavorite(){
    _iconData = Icons.favorite;
    _colorIcon = ColorsApp.COLOR_YELLOW;
    setState(() {
      
    });
  }
}