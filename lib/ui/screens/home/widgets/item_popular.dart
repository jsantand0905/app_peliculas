import 'package:flutter/material.dart';
import 'package:movieapp/design_system/colors.dart';
import 'package:movieapp/design_system/enumarables.dart';
import 'package:movieapp/design_system/responsive_design.dart';
import 'package:movieapp/design_system/spacing.dart';
import 'package:movieapp/design_system/text_app.dart';
import 'package:movieapp/design_system/tipography.dart';
import 'package:movieapp/domain/model/home/popular_model.dart'; 
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class ItemPopular extends StatelessWidget {

  final PopularModel? popularModel;
  static late ResponsiveDesign _responsiveDesign;

  const ItemPopular({
    Key? key,
    this.popularModel
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _responsiveDesign = ResponsiveDesign(context);
    return Container(
      width: _responsiveDesign.widthMultiplier(100),
      margin: EdgeInsets.only(right: _responsiveDesign.widthMultiplier(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //Image.network(Endpoints.baseUrlImage + 'w500/vC324sdfcS313vh9QXwijLIHPJp.jpg')
          TextApp(
            textType: TextType.White, 
            size: TypographyApp.FONT_SIZE_H5, 
            text: popularModel!.name!,
          ),
          SizedBox(
            height: _responsiveDesign.heightMultiplier(Spacing.SPACE_SM),
          ),
          RatingBar.builder(
            itemSize: TypographyApp.FONT_SIZE_H5,
            itemCount: 5,
            initialRating: popularModel!.voteAverage!/2,
            glowColor: ColorsApp.COLOR_WITHE,
            itemBuilder:  (context, _) => Icon(
              Icons.star,
              color: ColorsApp.COLOR_YELLOW,
            ), 
            onRatingUpdate: (value){}
          )
        ],
      ),
    );
  }
}