import 'package:flutter/material.dart';
import 'package:movieapp/config/app_config.dart';
import 'package:movieapp/design_system/enumarables.dart';
import 'package:movieapp/design_system/page_template/models/app_bar_model.dart';
import 'package:movieapp/design_system/page_template/page_template.dart';
import 'package:movieapp/design_system/responsive_design.dart';
import 'package:movieapp/design_system/skeleton.dart';
import 'package:movieapp/design_system/spacing.dart';
import 'package:movieapp/design_system/text_app.dart';
import 'package:movieapp/design_system/tipography.dart';
import 'package:movieapp/domain/model/home/popular_model.dart';
import 'package:movieapp/ui/models/ui_app_model.dart';
import 'package:movieapp/domain/model/home/response_popular.dart';
import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/ui/screens/home/interface/popular_interface.dart';
import 'package:movieapp/ui/screens/home/presenter/popular_presenter.dart';
import 'package:movieapp/ui/screens/home/widgets/item_popular.dart';
import 'package:movieapp/ui/screens/home/widgets/item_recomendations.dart';

class HomeScreen extends StatefulWidget {

  final AppConfig config;
  final Map<String, dynamic> language;

  HomeScreen({
    Key? key,
    required this.config,
    required this.language
  }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> implements PopularInterface{

  ResponsePopular? _responsePopular;
  ResponsePopular? _responseRecomendations;
  List<PopularModel> _populars = List.empty(growable:true);
  List<PopularModel> _recomendations = List.empty(growable:true);
  late ResponsiveDesign _responsiveDesign;
  bool _loadingPopular = true;
  bool _lodaingRecomendations = true;
  UiAppModel _appModel = UiAppModel();
  late PopularPresenter _popularPresenter;
  ScrollController _scrollControllerHorizontal = ScrollController();
  int _pagePopulars = 1;
  bool _viewSkeletonPopular = true;
  ScrollController _scrollControllerVertical = ScrollController();
  int _pageRecomendations = 1;
  bool _viewSkeletonRecomendations = true;

  @override
  void initState() {
    _popularPresenter = PopularPresenter(widget.config,this);
    _popularPresenter.getUi(widget.language);
    _popularPresenter.getPopulars(_pagePopulars.toString());
    _popularPresenter.getRecomendations(_pageRecomendations.toString());
    addListenerScrollHorizontal();
    addListenerScrollVertical();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _responsiveDesign = ResponsiveDesign(context);
    return PageTemplate(
      bodyTemplate:Padding(
        padding: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          controller: _scrollControllerVertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextApp(
                    textType: TextType.White, 
                    size: TypographyApp.FONT_SIZE_H4, 
                    text: _appModel.uiHomeModel!.popular),
              SizedBox(
                height: _responsiveDesign.heightMultiplier(Spacing.SPACE_MD),
              ),
               _loadingPopular && _viewSkeletonPopular ? _getSkeletonPopular() : Column(
                children: [
                  SingleChildScrollView(
                    controller: _scrollControllerHorizontal,
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: _getPopular(),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: _responsiveDesign.heightMultiplier(Spacing.SPACE_MD),
              ),
              TextApp(
                textType: TextType.White, 
                size: TypographyApp.FONT_SIZE_H4, 
                text: _appModel.uiHomeModel!.recomendations),
               _lodaingRecomendations && _viewSkeletonRecomendations ? _getSkeletonPopular() : Column(
                children: [_getRecomendations()]
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget>_getPopular(){
    List<Widget> response = List.empty(growable: true);
    _populars.forEach((element) { 
      response.add(ItemPopular(
        popularModel: element,
      ));
    });
    return response;
  }

  Column _getRecomendations(){
    List<Widget> response = List.empty(growable: true);
    _responseRecomendations!.results.forEach((element) { 
      response.add(ItemRecomendation(
        recomedationsModel: element,
        clickFavorite: (){},
      ));
    });
    return Column(
      children: response,
    );
  }

  Widget _getSkeletonPopular() => Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Skeleton(width: _responsiveDesign.widthMultiplier(120)),
      SizedBox(
        height: _responsiveDesign.heightMultiplier(Spacing.SPACE_SM),
      ),
      Skeleton(width: _responsiveDesign.widthMultiplier(90))
    ],
  );

  void addListenerScrollHorizontal() {
    _viewSkeletonPopular = false;
    _scrollControllerHorizontal.addListener(() {
      if (_scrollControllerHorizontal.offset >=
              _scrollControllerHorizontal.position.maxScrollExtent &&
          !_scrollControllerHorizontal.position.outOfRange) {
        _pagePopulars = _pagePopulars + 1;
        if (_pagePopulars <= _responsePopular!.totalPages) {
          _popularPresenter.getPopulars(_pagePopulars.toString());
        }
      }
    });
  }

  void addListenerScrollVertical() {
    _viewSkeletonRecomendations = false;
    _scrollControllerVertical.addListener(() {
      if (_scrollControllerVertical.offset >=
              _scrollControllerVertical.position.maxScrollExtent &&
          !_scrollControllerVertical.position.outOfRange) {
        _pageRecomendations = _pageRecomendations + 1;
        if (_pageRecomendations <= _responseRecomendations!.totalPages) {
          _popularPresenter.getRecomendations(_pageRecomendations.toString());
        }
      }
    });
  }

  @override
  void getInformation(ResponsePopular model, int? flag) {
    if(flag == null){
      _responsePopular = model;
      _populars.addAll( model.results);
      _getPopular();
    }else{
      _responseRecomendations = model;
      _recomendations.addAll( model.results);
      _getRecomendations();
    }
    
    setState(() {
      
    });
  }

  @override
  void hideLoading(int? flag) {
    if(flag == null){
    _loadingPopular = false;
    }else{
      _lodaingRecomendations = false;
    }
    setState(() {
      
    });
  }

  @override
  void setUi(UiAppModel model) {
    _appModel = model;
    setState(() {
      
    });
  }

  @override
  void showError(ErrorItem error) {
    print(error);
  }

  @override
  void showLoading(int? flag) {
    if(flag == null){
      _loadingPopular = true;
    }else{
      _lodaingRecomendations = true;
    }

    setState(() {
      
    });
  }
}