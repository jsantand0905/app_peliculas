import 'package:dartz/dartz.dart';
import 'package:movieapp/config/app_config.dart';
import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/domain/model/home/response_popular.dart';
import 'package:movieapp/ui/interface/base_interface.dart';
import 'package:movieapp/ui/presenter/base_presenter.dart';
import 'package:movieapp/ui/screens/home/interface/popular_interface.dart';

class PopularPresenter extends BasePresenterApp{

  AppConfig _config;
  PopularInterface _interface;

  PopularPresenter(this._config,this._interface) : super(_interface);

  Future<void> getPopulars(String page) async {
    _interface.showLoading(null);
    final Either<ErrorItem, ResponsePopular> response =
        await _config.appUseCase.getPopulars(page);

    response.fold((error) {
      _interface.hideLoading(null);
      _interface.showError(error);
    }, (model) {
      _interface.getInformation(model, null);
      _interface.hideLoading(null);
    });
  }

  Future<void> getRecomendations(String page) async {
    _interface.showLoading(1);
    final Either<ErrorItem, ResponsePopular> response =
        await _config.appUseCase.getRecomendations(page);

    response.fold((error) {
      _interface.hideLoading(1);
      _interface.showError(error);
    }, (model) {
      _interface.getInformation(model,1);
      _interface.hideLoading(1);
    });
  }

}