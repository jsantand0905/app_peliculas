import 'package:flutter/material.dart';
import 'package:movieapp/config/app_config.dart';
import 'package:movieapp/design_system/button_app.dart';
import 'package:movieapp/design_system/enumarables.dart';
import 'package:movieapp/design_system/page_template/models/app_bar_model.dart';
import 'package:movieapp/design_system/page_template/page_template.dart';
import 'package:movieapp/design_system/responsive_design.dart';
import 'package:movieapp/design_system/spacing.dart';
import 'package:movieapp/design_system/text_app.dart';
import 'package:movieapp/design_system/tipography.dart';
import 'package:movieapp/ui/interface/base_interface.dart';
import 'package:movieapp/ui/models/ui_app_model.dart';
import 'package:movieapp/ui/presenter/base_presenter.dart';
import 'package:movieapp/ui/screens/login/login.dart';

class WelcomeScreen extends StatefulWidget {

  final AppConfig config;
  static String welcomeRoute = '/welcome';
  final Map<String, dynamic> language;

  WelcomeScreen({
    Key? key,
    required this.config,
    required this.language
  }) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> implements BaseInterface {

  late BasePresenterApp _basePresenterApp;
  UiAppModel _uiAppModel = UiAppModel();
  late ResponsiveDesign _responsiveDesign; 

  @override
  void initState() {
    _basePresenterApp = BasePresenterApp(this);
    _basePresenterApp.getUi(widget.language);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _responsiveDesign = ResponsiveDesign(context);
    return PageTemplate(
      viewImageBackground: true,
      bodyTemplate: SingleChildScrollView(
        child: Column(
          children: [ 
            SizedBox(
              height: _responsiveDesign.heightMultiplier(Spacing.SPACE_XXL),
            ),
            TextApp(
              textType: TextType.White,
              size: TypographyApp.FONT_SIZE_H3,
              text: _uiAppModel.uiWelcomeModel!.welcome + "!",
              align: TextAlign.center ,
            ),
            Center(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ButtonApp(
                      pressed: (){}, 
                      value: _uiAppModel.uiWelcomeModel!.signUp,
                      typeButton: ButtonType.Primary,
                      width: _responsiveDesign.widthMultiplier(180),
                    ),
                    SizedBox(
                      height: _responsiveDesign.heightMultiplier(Spacing.SPACE_MD),
                    ),
                    ButtonApp(
                      pressed: ()=> _goToLogin(), 
                      value: _uiAppModel.uiWelcomeModel!.login,
                      typeButton: ButtonType.Secondary,
                      width: _responsiveDesign.widthMultiplier(180),
                    ),
                    SizedBox(
                      height: _responsiveDesign.heightMultiplier(Spacing.SPACE_MD),
                    ),
                    TextApp(
                      textType: TextType.White,
                      size: TypographyApp.FONT_SIZE_H5,
                      text: _uiAppModel.uiWelcomeModel!.forgotPassword,
                      align: TextAlign.center,
                      decoration: TextdecorationApp.Underline,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void setUi(UiAppModel model) {
    _uiAppModel = model;
    setState(() {
      
    });
  }

  void _goToLogin(){
     Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen(
            config: widget.config,
            language: widget.language,
          )),(value){return false;});
  }

  
}