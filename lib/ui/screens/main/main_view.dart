import 'package:flutter/material.dart';
import 'package:movieapp/config/app_config.dart';
import 'package:movieapp/design_system/colors.dart';
import 'package:movieapp/design_system/page_template/models/app_bar_model.dart';
import 'package:movieapp/design_system/page_template/page_template.dart';
import 'package:movieapp/ui/helpers/utils/contstants_botton_navigation_bar.dart';
import 'package:movieapp/ui/interface/base_interface.dart';
import 'package:movieapp/ui/models/ui_app_model.dart';
import 'package:movieapp/ui/presenter/base_presenter.dart';
import 'package:movieapp/ui/screens/home/home.dart';

class MainView extends StatefulWidget {

  final AppConfig config;
  final Map<String, dynamic> language;

  MainView({
    Key? key,
    required this.config,
    required this.language
  }) : super(key: key);

  @override
  State<MainView> createState() => _MainViewState();
}

class _MainViewState extends State<MainView> implements BaseInterface{

  Widget? _bodyView;
  int currentIndex = 0;
  UiAppModel _uiAppModel = UiAppModel();
  late BasePresenterApp _presenter;
  String _header  = '';

  @override
  void initState() {
    _presenter = BasePresenterApp(this);
    _presenter.getUi(widget.language);
    voidChangeView(currentIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      appBar: AppBarModel(_header),
      bodyTemplate: _bodyView,
      bottomNavBar: BottomNavigationBar(
        onTap: (value) => voidChangeView(value),
        currentIndex: currentIndex,
        type: BottomNavigationBarType.fixed,
        fixedColor: ColorsApp.COLOR_YELLOW,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: _uiAppModel.bottomNavBarModel!.home),
          BottomNavigationBarItem(
              icon: Icon(Icons.favorite_outline), label: _uiAppModel.bottomNavBarModel!.favorites),
          BottomNavigationBarItem(
              icon: Icon(Icons.replay), label: _uiAppModel.bottomNavBarModel!.recent),
          BottomNavigationBarItem(
              icon: Icon(Icons.search_outlined), label: _uiAppModel.bottomNavBarModel!.search)
        ],
      ) 
    );
  }

  voidChangeView(int parameter) async {
    switch (parameter) {
      case ConstantsBottonNavBar.home:
        _bodyView = HomeScreen(config: widget.config, language: widget.language);
        _header = _uiAppModel.bottomNavBarModel!.home;
        break;
    }

    currentIndex = parameter;
    setState(() {});
  }

  @override
  void setUi(UiAppModel model) {
    _uiAppModel = model;
    setState(() {
      
    });
  }
}