class UiHomeModel {
  String popular;
  String recomendations;
  String watchNow;

  UiHomeModel(
    this.popular,
    this.recomendations,
    this.watchNow
  );
}