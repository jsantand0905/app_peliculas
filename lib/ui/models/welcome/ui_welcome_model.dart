class UiWelcomeModel{
  String welcome;
  String signUp;
  String login;
  String forgotPassword;

  UiWelcomeModel(
    this.welcome,
    this.signUp,
    this.login,
    this.forgotPassword
  );
}