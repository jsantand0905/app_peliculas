class BottomNavBarModel {
  String home;
  String favorites;
  String recent;
  String search;

  BottomNavBarModel(
    this.home,
    this.favorites,
    this.recent, this.search
  );
}