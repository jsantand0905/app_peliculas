class UiLoginModel {
  String name;
  String password;
  String messageError;

  UiLoginModel(
    this.name,
    this.password,
    this.messageError
  );
}