import 'package:movieapp/ui/models/home/ui_home_model.dart';
import 'package:movieapp/ui/models/login/ui_login_model.dart';
import 'package:movieapp/ui/models/main_view/bottom_nav_bar_model.dart';
import 'package:movieapp/ui/models/welcome/ui_welcome_model.dart';

class UiAppModel{
  UiWelcomeModel? uiWelcomeModel;
  UiLoginModel? uiLoginModel;
  BottomNavBarModel? bottomNavBarModel;
  UiHomeModel? uiHomeModel;

  UiAppModel({
    this.uiWelcomeModel,
    this.uiLoginModel,
    this.bottomNavBarModel,
    this.uiHomeModel
    }
  );
}