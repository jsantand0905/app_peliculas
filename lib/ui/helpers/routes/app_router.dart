
import 'package:flutter/material.dart';
import 'package:movieapp/ui/helpers/routes/welcome_screen_routes.dart';
import 'package:movieapp/ui/screens/welcome/welcome.dart';


class RouterApp {
  Map<String, WidgetBuilder> routes() {
    final Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{}
      ..addAll(<String, WidgetBuilder>{
        WelcomeScreen.welcomeRoute : WelcomeScreenRoutes.getWelcomeScreen
      });
    return routes;
  }
}
