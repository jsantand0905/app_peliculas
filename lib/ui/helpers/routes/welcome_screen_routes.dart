import 'package:flutter/cupertino.dart';
import 'package:movieapp/config/app_config.dart';
import 'package:movieapp/config/localizations/app_localizations.dart';
import 'package:movieapp/infraestructure/app_api.dart';
import 'package:movieapp/ui/screens/welcome/welcome.dart';

class WelcomeScreenRoutes {
  WelcomeScreenRoutes._();

  static WelcomeScreen getWelcomeScreen(BuildContext context){
    return WelcomeScreen(
      config: AppConfig(AppApi(context)),
      language: AppMovieLocalizations.of(context)!.getJsonTranslate(),
    );
  }
}