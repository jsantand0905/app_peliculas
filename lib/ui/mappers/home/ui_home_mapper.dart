import 'package:movieapp/design_system/mapper.dart';
import 'package:movieapp/ui/models/home/ui_home_model.dart';

class UiHomeMapper implements Mapper<UiHomeModel>{
  @override
  UiHomeModel fromMap(Map<String, dynamic> json) => UiHomeModel(
    json['popular'],
    json['recomendations'],
    json['watch_now']
  );

  @override
  Map<String, dynamic>? toMap(UiHomeModel data) {
    throw UnimplementedError();
  }

}