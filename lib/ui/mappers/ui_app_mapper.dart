

import 'package:movieapp/design_system/mapper.dart';
import 'package:movieapp/ui/mappers/home/ui_home_mapper.dart';
import 'package:movieapp/ui/mappers/login/ui_login_mapper.dart';
import 'package:movieapp/ui/mappers/main_view/botton_nav_bar_mapper.dart';
import 'package:movieapp/ui/mappers/welcome/ui_welcome_mapper.dart';
import 'package:movieapp/ui/models/ui_app_model.dart';

class UiAppMapper implements Mapper<UiAppModel>{
  @override
  UiAppModel fromMap(Map<String, dynamic> json) => UiAppModel(
   uiWelcomeModel: UiWelcomeMapper().fromMap(json['welcome_view']),
   uiLoginModel: UiLoginMapper().fromMap(json['login_view']),
   bottomNavBarModel: BottomNavBarMapper().fromMap(json['bottom_nav_bar']) ,
   uiHomeModel: UiHomeMapper().fromMap(json['home_view'])
  );

  @override
  Map<String, dynamic>? toMap(UiAppModel data) {
    throw UnimplementedError();
  }

}