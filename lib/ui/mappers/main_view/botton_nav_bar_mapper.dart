import 'package:movieapp/design_system/mapper.dart';
import 'package:movieapp/ui/models/main_view/bottom_nav_bar_model.dart';

class BottomNavBarMapper implements Mapper<BottomNavBarModel>{
  @override
  BottomNavBarModel fromMap(Map<String, dynamic> json) => BottomNavBarModel(
    json['home'],
    json['favorites'],
    json['recent'],
    json['search']
  );

  @override
  Map<String, dynamic>? toMap(BottomNavBarModel data) {
    throw UnimplementedError();
  }

}