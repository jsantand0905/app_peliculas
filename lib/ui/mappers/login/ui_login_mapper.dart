import 'package:movieapp/design_system/mapper.dart';
import 'package:movieapp/ui/models/login/ui_login_model.dart';

class UiLoginMapper implements Mapper<UiLoginModel>{
  @override
  UiLoginModel fromMap(Map<String, dynamic> json) => UiLoginModel(
    json['name'],
    json['password'],
    json['message_error']
  );

  @override
  Map<String, dynamic>? toMap(UiLoginModel data) {
    throw UnimplementedError();
  }

}