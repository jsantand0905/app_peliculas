import 'package:movieapp/design_system/mapper.dart';
import 'package:movieapp/ui/models/welcome/ui_welcome_model.dart';

class UiWelcomeMapper implements Mapper<UiWelcomeModel>{
  @override
  UiWelcomeModel fromMap(Map<String, dynamic> json) => UiWelcomeModel(
    json['welcome'],
    json['sign_up'],
    json['login'],
    json['forgot_password']
  );

  @override
  Map<String, dynamic>? toMap(UiWelcomeModel data) {
    throw UnimplementedError();
  }

}