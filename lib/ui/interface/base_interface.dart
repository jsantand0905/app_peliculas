
import 'package:movieapp/ui/models/ui_app_model.dart';

abstract class BaseInterface {
  void setUi(UiAppModel model);
}