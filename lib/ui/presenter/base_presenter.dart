
import 'package:movieapp/ui/interface/base_interface.dart';
import 'package:movieapp/ui/mappers/ui_app_mapper.dart';

class BasePresenterApp {
  BaseInterface _interface;
  UiAppMapper _mapper = UiAppMapper();

  BasePresenterApp(this._interface);
  
  void getUi(Map<String, dynamic> language) {
    _interface.setUi(_mapper.fromMap(language));
  }

}