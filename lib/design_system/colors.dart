

import 'dart:ui' show Color;

import 'package:flutter/material.dart' show Colors;

class ColorsApp {
  static const Color COLOR_YELLOW = Color(0xFFFFD233);
  static const Color COLOR_BLACK = Color(0xFF191919);
  static const Color COLOR_GRAY = Color(0xFF8C8C8C);
  static const Color COLOR_WITHE = Color(0xFFFFFFFF);
  static const Color BG_DARK_GRAY = Color(0xFFCCCCCC);
}
