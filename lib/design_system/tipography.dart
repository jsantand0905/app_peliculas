

class TypographyApp {
  static const double FONT_SIZE_H1 = 40;
  static const double FONT_SIZE_H2 = 32;
  static const double FONT_SIZE_H3 = 28;
  static const double FONT_SIZE_H4 = 24;
  static const double FONT_SIZE_H5 = 20;
  static const double FONT_SIZE_H6 = 18;
  static const double FONT_SMALL = 16;
  static const double FONT_XS = 12;
}
