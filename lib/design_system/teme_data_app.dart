import 'package:flutter/material.dart';
import 'package:movieapp/design_system/colors.dart';

class ThemeDataApp {
  static ThemeData lightTheme = ThemeData(
    primaryColor: ColorsApp.COLOR_BLACK,
    cardColor: ColorsApp.COLOR_WITHE,
    brightness: Brightness.dark,
    focusColor: ColorsApp.COLOR_YELLOW,
    appBarTheme: _getAppBarTheme(),
    colorScheme: _getColorScheme().copyWith(secondary: ColorsApp.COLOR_WITHE),
    snackBarTheme: SnackBarThemeData(
      actionTextColor: ColorsApp.COLOR_WITHE,
      contentTextStyle: TextStyle(
        color: ColorsApp.COLOR_WITHE
      )
    ),
    bottomAppBarTheme: BottomAppBarTheme(
      color: ColorsApp.COLOR_YELLOW
    )
  );

  static ColorScheme _getColorScheme() => const ColorScheme.dark(
        primary: ColorsApp.COLOR_YELLOW,
        secondary: ColorsApp.COLOR_YELLOW,
        primaryVariant: ColorsApp.COLOR_YELLOW,
        secondaryVariant: ColorsApp.COLOR_YELLOW,
        surface: ColorsApp.COLOR_WITHE,
        background: ColorsApp.COLOR_BLACK,
        onPrimary: ColorsApp.COLOR_BLACK,
        onSecondary: ColorsApp.COLOR_BLACK,
        onSurface: ColorsApp.COLOR_BLACK,
        onError: ColorsApp.COLOR_GRAY,
        onBackground: ColorsApp.COLOR_BLACK,
      );

  static AppBarTheme _getAppBarTheme() => const AppBarTheme(
    brightness: Brightness.dark,
    titleTextStyle: TextStyle(color: Colors.white)
  );
}