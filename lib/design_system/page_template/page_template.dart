import 'package:flutter/material.dart';
import 'package:movieapp/design_system/colors.dart';
import 'package:movieapp/design_system/page_template/models/app_bar_model.dart';

class PageTemplate extends StatelessWidget {

  final AppBarModel? appBar;
  final BottomNavigationBar? bottomNavBar;
  final Widget? bodyTemplate;
  final bool viewImageBackground;
  const PageTemplate({
    Key? key,
    this.appBar,
    this.bottomNavBar,
    this.bodyTemplate,
    this.viewImageBackground = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar != null ? AppBar(
        elevation: 0.0,
        bottomOpacity: 0.0,
        title: 
          Center(
            child: Text(
              appBar!.title,
              style: TextStyle(
                color: ColorsApp.COLOR_GRAY,
                fontWeight: FontWeight.w300
              ),
            ),
          ),
        actions: [
          IconButton(
            onPressed: (){}, 
            icon: Icon(
              Icons.settings_outlined,
              color: ColorsApp.COLOR_GRAY,
            )
          )
        ],
      ) : null,
      body: viewImageBackground ? Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image:  DecorationImage(
            image: AssetImage("assets/images/image_background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: bodyTemplate
      ) : Container(
        width: MediaQuery.of(context).size.width,
        color: ColorsApp.COLOR_BLACK,
        child: bodyTemplate,
      ),
      bottomNavigationBar: bottomNavBar ?? null,
    );
  }
}