import 'package:flutter/material.dart';
import 'package:movieapp/design_system/colors.dart';



class Skeleton extends StatefulWidget {
  final double height;
  final double width;

  const Skeleton({
    Key? key,
    this.height = 10,
    this.width = 200,
  }) : super(key: key);

  @override
  _BcSkeletonState createState() => _BcSkeletonState();
}

class _BcSkeletonState extends State<Skeleton>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  late Animation gradientPosition;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(milliseconds: 1500),
      vsync: this,
    );

    gradientPosition = Tween<double>(
      begin: -3,
      end: 10,
    ).animate(
      CurvedAnimation(parent: _controller, curve: Curves.linear),
    );

    _controller.repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => AnimatedBuilder(
        animation: gradientPosition,
        builder: (BuildContext context, Widget? snapshot) => Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              20,
            ),
            color: ColorsApp.COLOR_WITHE,
            gradient: LinearGradient(
              begin: Alignment(gradientPosition.value, 0),
              end: const Alignment(-1, 0),
              colors: const <Color>[
                Colors.white38,
                Colors.white10,
                Colors.white38
              ],
            ),
          ),
        ),
      );
}
