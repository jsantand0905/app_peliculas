
import 'package:flutter/material.dart';
import 'package:movieapp/design_system/colors.dart';
import 'package:movieapp/design_system/enumarables.dart';
import 'package:movieapp/design_system/text_app.dart';
import 'package:movieapp/design_system/tipography.dart';

class ButtonApp extends StatelessWidget {

  final VoidCallback pressed;
  final ButtonType? typeButton;
  final String value;
  final double? width;

  const ButtonApp({
    Key? key,
    required this.pressed,
    required this.value,
    this.typeButton,
    this.width
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>pressed.call(),
      child: Container(
        width: width ?? null,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: typeButton == ButtonType.Primary ? ColorsApp.COLOR_YELLOW : ColorsApp.COLOR_WITHE
        ),
        child: TextApp(
          textType: TextType.Black,
          size: TypographyApp.FONT_SIZE_H4,
          text: value,
          align: TextAlign.center ,
        )
      ),
    );
  }
}