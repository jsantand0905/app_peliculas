import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SnackbarMessage {
  late String message;
  late BuildContext context;
  SnackbarMessage(this.context);

  void showSnackbar({message = 'Error', bool action = false, String labelAction = 'Ok', Function? onPressedAction}){
    this.message = message;
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        content: Text(message!),
        action: action ? SnackBarAction(label: labelAction, onPressed: onPressedAction!()) : null,
      ),
    );
  }
}