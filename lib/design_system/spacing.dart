
class Spacing {
  static const double SPACE_XXS = 2;
  static const double SPACE_XS = 4;
  static const double SPACE_SM = 8;
  static const double SPACE_SL = 12;
  static const double SPACE_MD = 16;
  static const double SPACE_LG = 24;
  static const double SPACE_XL = 32;
  static const double SPACE_XXL = 48;
  static const double SPACE_XXXL = 60;
  static const double SPACE_XXXXL = 100;
  static const double SPACE_XXXXXL = 150;
}
