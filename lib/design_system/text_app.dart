import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:movieapp/design_system/colors.dart';
import 'package:movieapp/design_system/enumarables.dart';

class TextApp extends StatelessWidget {

  final TextType textType;
  final double size;
  final String text;
  final TextAlign?  align;
  final TextdecorationApp? decoration;
  const TextApp({
    Key? key,
    required this.textType,
    required this.size,
    required this.text,
    this.align,
    this.decoration
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: align ?? TextAlign.left,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontWeight: FontWeight.w400,
        color: textType == TextType.Black ? ColorsApp.COLOR_BLACK :
        textType == TextType.White ? ColorsApp.COLOR_WITHE : ColorsApp.COLOR_GRAY,
        fontSize: size, 
        decoration: decoration == TextdecorationApp.Underline ? TextDecoration.underline :TextDecoration.none,
      ),
    );
  }
}