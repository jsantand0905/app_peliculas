import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_localizations_delegate.dart';

class AppMovieLocalizations {
  final Locale locale;

  AppMovieLocalizations(this.locale);

  static AppMovieLocalizations? of(BuildContext context) =>
      Localizations.of<AppMovieLocalizations>(context, AppMovieLocalizations);

  static const LocalizationsDelegate<AppMovieLocalizations> delegate =
      AppLocalizationsDelegate();

  Map<String, dynamic>? _localizedStrings;

  Future<bool> load() async {
    String jsonString = await rootBundle
        .loadString('assets/locale/es/app_strings.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value);
    });

    return true;
  }

  String translate(String key) => _localizedStrings![key];

  Map<String, dynamic> getJsonTranslate() => _localizedStrings!;
}
