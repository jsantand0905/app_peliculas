import 'package:flutter/material.dart';
import 'package:movieapp/config/localizations/app_localizations.dart';


class AppLocalizationsDelegate
    extends LocalizationsDelegate<AppMovieLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<AppMovieLocalizations> load(Locale locale) async {
    AppMovieLocalizations localizations = new AppMovieLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(AppLocalizationsDelegate old) => false;
}
