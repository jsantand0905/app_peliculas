import '../domain/model/gateway/app_gateway.dart';
import '../domain/usecase/app_usecase.dart';

class AppConfig {
  late AppUseCase appUseCase;

  AppConfig([AppGateway? _api]){
    appUseCase = AppUseCase(_api);
  }

}