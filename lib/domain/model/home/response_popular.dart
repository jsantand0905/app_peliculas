import 'package:movieapp/domain/model/home/popular_model.dart';

class ResponsePopular {
  int page;
  List<PopularModel> results;
  int totalPages;
  int totalResults;

  ResponsePopular(
    this.page,
    this.results,
    this.totalPages,
    this.totalResults
  );
}