class PopularModel {
  String? posterPath;
  double? popularity;
  int? id;
  String? backdropPath;
  double? voteAverage;
  String? overView;
  String? firstAirDate;
  String? originalLanguage;
  int? voteCount;
  String? name;
  String? originalName;

  PopularModel(
    this.posterPath,
    this.popularity,
    this.id,
    this.backdropPath,
    this.voteAverage,
    this.overView,
    this.firstAirDate,
    this.originalLanguage,
    this.voteCount,
    this.name,
    this.originalName
  );
}