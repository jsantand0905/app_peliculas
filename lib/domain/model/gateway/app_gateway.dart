import 'package:dartz/dartz.dart';
import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/domain/model/home/response_popular.dart';

abstract class AppGateway {

  Future<Either<ErrorItem, ResponsePopular>> getPopulars(String page);
  Future<Either<ErrorItem, ResponsePopular>> getRecomendations(String page);
}
