
import '../model/gateway/app_gateway.dart';

class AppUseCase {
  final AppGateway? _gateway;

  AppUseCase(this._gateway);

  Future getPopulars(String page) async =>
      await this._gateway!.getPopulars(page);
  Future getRecomendations(String page) async =>
      await this._gateway!.getRecomendations(page);
}
