class ErrorResponse {
  String? message;

  ErrorResponse({this.message });

  factory ErrorResponse.fromJson(Map<String, dynamic> json) {
    return ErrorResponse(
      message: json['msg'] ?? '',
    );
  }
}
