
import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/infraestructure/models/base_exception.dart';

class ServerException extends BaseException {
  ServerException(ErrorItem errorItem) : super(errorItem);
}