

import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/infraestructure/models/base_exception.dart';

class BusinessException extends BaseException {
  BusinessException(ErrorItem errorItem) : super(errorItem);
}