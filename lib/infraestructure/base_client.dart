import 'dart:convert';
import 'dart:io';

import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/infraestructure/models/server_exepction.dart';
import 'package:movieapp/infraestructure/utils/endpoints.dart';


class BaseClient {

  HttpClient _createClient() {
    final HttpClient httpClient = HttpClient();
    httpClient
      ..idleTimeout = const Duration(seconds: 10)
      ..connectionTimeout = const Duration(seconds: 10);
    return httpClient;
  }

  Future<Map<String, dynamic>?> sendAsync(
      String service,
      Map<String, dynamic>? data,
      Map<String, dynamic>? additionalHeaders,
      Map<String, dynamic>? queryParams) async {
    String responseQueryParams = '';
    if (queryParams != null) {
      responseQueryParams = parseQueryParams(queryParams);
    }
    String relativeUrl =  Endpoints.baseUrl+
            service +
            responseQueryParams;
    Uri pathHttp = Uri.parse(relativeUrl);

    HttpClient _httpClient = _createClient();
    HttpClientRequest request;
    request = await _httpClient.getUrl(pathHttp);

    return await _makeRequest(request, data, additionalHeaders);
  }

  Future<Map<String, dynamic>?> _makeRequest(
      HttpClientRequest request,
      Map<String, dynamic>? data,
      Map<String, dynamic>? additionalHeaders) async {
    String contentTypeStr = 'application/json';

    request.headers.removeAll(HttpHeaders.acceptEncodingHeader);
    request.headers.set(HttpHeaders.cacheControlHeader, 'no-cache');
    request.headers.set(HttpHeaders.contentTypeHeader, contentTypeStr);

    if (additionalHeaders != null) {
      additionalHeaders.forEach((key, value) {
        request.headers.set(key, value);
      });
    }

    if (data != null) {
      String dataStr = json.encode(data);
      List<int> bodyBytes = utf8.encode(dataStr);
      request.headers.set('Content-Length', bodyBytes.length.toString());
      request.add(bodyBytes);
    }

    HttpClientResponse response = await request.close();

    return await _validateResponse(response);
  }

  Future<Map<String, dynamic>> _validateResponse(
      HttpClientResponse response) async {
    String responseStr;
    responseStr = await response.transform(utf8.decoder).join();
    if (response.statusCode == 200) {
      final Map<String, dynamic> responseMap = jsonDecode(responseStr);
      return responseMap;
    }

    String generalError;

    switch (response.statusCode) {
      case HttpStatus.requestTimeout:

      case HttpStatus.gatewayTimeout:
        generalError = 'Al parecer hay inconvenientes '
            'en los servicios, por favor intenta de nuevo';
        break;

      case HttpStatus.networkConnectTimeoutError:
        generalError = 'Error conexión a internet, por favor intenta de nuevo';
        break;

      default:
        generalError = 'Ha ocurrido un error inesperado'
            ' en la transacción, por favor intenta de nuevo';
        break;
    }

    throw ServerException(ErrorItem(
      message: generalError,
    ));
  }

  String parseQueryParams(Map<String, dynamic> input) {
    String response = '?';
    input.forEach((key, value) {
      response = response + key + '=' + value + '&';
    });
    return response.substring(0, response.length - 1);
  }
}
