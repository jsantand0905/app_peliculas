import 'package:movieapp/design_system/mapper.dart';
import 'package:movieapp/domain/model/home/popular_model.dart';
import 'package:movieapp/domain/model/home/response_popular.dart';
import 'package:movieapp/infraestructure/mappers/home/popular_mapper.dart';

class ResponsePopularMapper implements Mapper<ResponsePopular>{
  @override
  ResponsePopular fromMap(Map<String, dynamic> json) => ResponsePopular(
    json['page'],
    List<PopularModel>.from(json['results'].map((x) => PopularMapper().fromMap(x))),
    json['total_pages'],
    json['total_results']
  );
  @override
  Map<String, dynamic>? toMap(ResponsePopular data) {
    // TODO: implement toMap
    throw UnimplementedError();
  }

}