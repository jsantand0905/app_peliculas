import 'package:movieapp/design_system/mapper.dart';
import 'package:movieapp/domain/model/home/popular_model.dart';

class PopularMapper implements Mapper<PopularModel> {
  @override
  PopularModel fromMap(Map<String, dynamic> json) => PopularModel(
    json['poster_path'],
    json['popularity'],
    json['id'],
    json['backdrop_path'],
    double.parse(json['vote_average'].toString()),
    json['overview'],
    json['first_air_date'],
    json['original_language'],
    json['vote_count'],
    json['name'],
    json['original_name']
  );

  @override
  Map<String, dynamic>? toMap(PopularModel data) {
    // TODO: implement toMap
    throw UnimplementedError();
  }

}