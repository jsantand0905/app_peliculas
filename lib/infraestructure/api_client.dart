import 'package:movieapp/infraestructure/base_client.dart';
import 'package:movieapp/infraestructure/helpers/request_api_helper.dart';

class ApiClient{
  late BaseClient _baseClient;
  late RequestApiHelper requestHelper;

  ApiClient() {
    _baseClient = BaseClient();
    requestHelper = RequestApiHelper();
  }

  Map<String, dynamic> _addHeaders(Map<String, dynamic>? additionalHeaders) {
    final Map<String, dynamic> headers = <String, dynamic>{};
    if (additionalHeaders != null) {
      headers.addAll(additionalHeaders);
    }
    return headers;
  }

  Future send(
    String service, {
    Map<String, dynamic>? data,
    Map<String, dynamic>? additionalHeaders,
    Map<String, dynamic>? queryParams,
  }) async {
    final Map<String, dynamic> _headers = _addHeaders(additionalHeaders);
    final Map<String, dynamic>? _body = data;
    final Map<String, dynamic>? response = await _baseClient
        .sendAsync( service, _body, _headers, queryParams);
    return response;
  }
}
