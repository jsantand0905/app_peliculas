class Endpoints {
  
  static const baseUrl = 'https://api.themoviedb.org/3/tv';
  static const getPopulars = '/popular';
  static const getRecomendations ='/top_rated';
  static const apikey = 'ab5ad3f1d1b33a16e3e06c0e9cf41e48';
  static const baseUrlImage = 'https://image.tmdb.org/t/p';
}
