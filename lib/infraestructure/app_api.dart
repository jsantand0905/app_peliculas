import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/domain/model/gateway/app_gateway.dart';
import 'package:movieapp/domain/model/home/response_popular.dart';
import 'package:movieapp/infraestructure/api_client.dart';
import 'package:movieapp/infraestructure/helpers/exception_helper.dart';
import 'package:movieapp/infraestructure/mappers/home/response_popular_mapper.dart';
import 'package:movieapp/infraestructure/models/base_exception.dart';
import 'package:movieapp/infraestructure/utils/endpoints.dart';


class AppApi implements AppGateway {

  late ApiClient apiClient;

  AppApi(BuildContext context){
    apiClient = ApiClient();
  }

  @override
  Future<Either<ErrorItem, ResponsePopular>> getPopulars(String page) async{

    try {
      Map<String, dynamic> response;
      final Map<String, dynamic> params = <String, dynamic>{
        'api_key' : Endpoints.apikey,
        'language' : 'en-US',
        'page': page,
      };
      response = await apiClient.send(Endpoints.getPopulars,queryParams: params);
      final ResponsePopular data = ResponsePopularMapper().fromMap(response);
      return Right<ErrorItem, ResponsePopular>(data);
    } on BaseException catch (e) {
      return Left<ErrorItem, ResponsePopular>(
          ExceptionHelper.handlerBaseExceptionToErrorItem(e));
    } on Exception catch (e) {
      final ErrorItem errorItem =
          ExceptionHelper.handlerBaseExceptionToErrorItem(e);
      return Left<ErrorItem, ResponsePopular>(errorItem);
    }

  }

  @override
  Future<Either<ErrorItem, ResponsePopular>> getRecomendations(String page) async{
    try {
      Map<String, dynamic> response;
      final Map<String, dynamic> params = <String, dynamic>{
        'api_key' : Endpoints.apikey,
        'language' : 'en-US',
        'page': page,
      };
      response = await apiClient.send(Endpoints.getRecomendations,queryParams: params);
      final ResponsePopular data = ResponsePopularMapper().fromMap(response);
      return Right<ErrorItem, ResponsePopular>(data);
    } on BaseException catch (e) {
      return Left<ErrorItem, ResponsePopular>(
          ExceptionHelper.handlerBaseExceptionToErrorItem(e));
    } on Exception catch (e) {
      final ErrorItem errorItem =
          ExceptionHelper.handlerBaseExceptionToErrorItem(e);
      return Left<ErrorItem, ResponsePopular>(errorItem);
    }
  }

  
}
