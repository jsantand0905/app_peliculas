

enum EnvironmentApp { Production, Testing, Mock }

enum HttpRequestType { Post, Patch, Put, Delete, Get }

enum HttpContentType { Json, FormUrlEncoded }

