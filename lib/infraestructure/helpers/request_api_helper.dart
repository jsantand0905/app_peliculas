

import 'package:flutter/material.dart';
import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/infraestructure/models/business_exception.dart';
import 'package:movieapp/infraestructure/models/error_response.dart';
import 'package:movieapp/infraestructure/models/server_exepction.dart';

class RequestApiHelper {



  RequestApiHelper();

  Map<String, dynamic> validateResponse(
      Map<String, dynamic> response, BuildContext context) {
    if (response.containsKey('error') || response.containsKey('errors')) {
      final ErrorResponse error = ErrorResponse.fromJson(response);
      final ErrorItem errorItem = ErrorItem(
        message: error.message,
      );
      throw BusinessException(errorItem);
    } else if (response.containsKey('error') &&
        response.containsKey('status')) {
      throw ServerException(ErrorItem(
          message: 'Error en servidor'));
    }
    return response;
  }
}
