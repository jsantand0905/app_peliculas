import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:movieapp/domain/model/error_item.dart';
import 'package:movieapp/infraestructure/models/base_exception.dart';


class ExceptionHelper {
  ExceptionHelper._();

  static ErrorItem handlerBaseExceptionToErrorItem(Exception exception,
      [ValueChanged<Exception>? reportError]) {
    ErrorItem errorItem;
    if (exception is BaseException) {
      errorItem = exception.getError();
    } else if (exception is SocketException) {
      errorItem = ErrorItem(
        message: 'Error de conectividad',
      );
    } else {
      errorItem = ErrorItem(
        message: 'Se ha producido un error',
      );
    }
    if (reportError != null) reportError(exception);

    return errorItem;
  }
}
