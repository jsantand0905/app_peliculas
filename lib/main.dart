import 'package:flutter/material.dart';
import 'package:movieapp/design_system/page_template/models/app_bar_model.dart';
import 'package:movieapp/design_system/page_template/page_template.dart';
import 'package:movieapp/design_system/teme_data_app.dart';
import 'package:movieapp/ui/helpers/routes/app_router.dart';
import 'package:movieapp/ui/screens/welcome/welcome.dart';
import 'config/localizations/app_localizations.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final RouterApp routes = RouterApp();
    return MaterialApp(
      title: 'Movie app',
      debugShowCheckedModeBanner: false,
      theme: ThemeDataApp.lightTheme,
      initialRoute: WelcomeScreen.welcomeRoute,
      routes: routes.routes(),
      localizationsDelegates: <LocalizationsDelegate<dynamic>>[
        AppMovieLocalizations.delegate
      ],
    );
  }
}

